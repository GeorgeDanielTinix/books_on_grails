package es.quindimil

class Book {
    String title
    String isbn
    
    static belongTo = [author: Author]
    

    static constraints = {
        isbn nullable: true
    }

    String toString(){
        title
    }
}
