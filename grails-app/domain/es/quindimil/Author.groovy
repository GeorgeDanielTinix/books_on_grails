package es.quindimil

class Author {

    String name
    static hasMany = [books: Book]

    String toString(){
        name
    }
}
